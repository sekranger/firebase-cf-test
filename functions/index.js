const functions = require('firebase-functions');

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });
exports.onCampaignsWrite = functions.database.ref('/campaigns/{campaign_id}')
.onWrite(event => {
  const campaign_id = event.params.campaign_id;
  const ss = event.data.exists() ? event.data : event.data.previous;
  const val = ss.val();
  const created_by_uid = val.created_by_uid;
  const campaignsByProfileRef = ss.adminRef.root.child('/campaigns_by_profile/' + created_by_uid + '/' + campaign_id);

  if (event.data.exists()) {
    return campaignsByProfileRef.update(val);
  } else {
    return campaignsByProfileRef.remove();
  }
});

exports.onOrdersWrite = functions.database.ref('/orders/{order_id}')
.onWrite(event => {
  const order_id = event.params.order_id;
  const ss = event.data.exists() ? event.data : event.data.previous;
  const val = ss.val();
  const campaign_id = val.campaign_id;
  const uid = val.uid;
  const card_id = val.card_id;

  if (event.data.exists()) {
    let updatingData = {}
    updatingData['/orders_by_campaign/' + campaign_id + '/' + order_id] = val;
    updatingData['/orders_by_profile/' + uid + '/' + order_id] = val;
    updatingData['/orders_by_card/' + card_id + '/' + order_id] = val;
    return ss.adminRef.root.update(updatingData, err => {if (err) {console.log('onOrdersWrite:', err)}});
  } else {
    const ordersByCampaign = ss.adminRef.root.child('/orders_by_campaign/' + campaign_id + '/' + order_id);
    const ordersByProfile = ss.adminRef.root.child('/orders_by_profile/' + uid + '/' + order_id);
    const ordersByCard = ss.adminRef.root.child('/orders_by_card/' + card_id + '/' + order_id);
    return Promise.all([ordersByCampaign, ordersByProfile, ordersByCard]);
  }
});

exports.onOrdersWrite2 = functions.database.ref('/orders/{order_id}')
.onWrite(event => {});